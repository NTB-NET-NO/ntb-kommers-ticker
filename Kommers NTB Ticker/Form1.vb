Imports System.Xml
Imports System.net
Imports System.Text
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Globalization
Imports System.io
' Imports System.Timers
' Imports ntb_FuncLib






Public Class frmNewsTicker

    Dim widthX As Single
    Dim heightY As Single
    Dim g As Graphics
    Dim xmlst As String      'string from the xml file
    Dim fo As Font





    Dim mFontSize As Integer

    
    Dim str As String
    Dim strwidth As SizeF    'gets the xml string's width and height
    Dim douX As Double      'stores the xmlstring's width     


    ' Henter variabler som skal brukes 
    Public UserName As String = My.Settings.RSSUserName
    Public PassWord As String = My.Settings.RSSPassWord
    Public RssPath As String = My.Settings.RSSServer

    Public FontSize As Integer

    Dim enUSCulture As New CultureInfo("en-US", True)




    ' Public TickerSpeed As Single = Convert.ToSingle(AppSettings("Speed"))
    Public TickerSpeed As Single = Val(My.Settings.TickerSpeed)


    ' Drag and move the ticker
    Private bFormDragging As Boolean = False
    Private oPointClicked As Point

   

    Private Sub PictureBox2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles PictureBox2.MouseClick
        If e.Button = Windows.Forms.MouseButtons.Right Then

            mnuPopup.Visible = True
            mnuPopup.Left = e.X
            mnuPopup.Top = Me.Location.Y
            Debug.Write(mnuPopup.Top.ToString)
        End If
    End Sub

 


    Private Sub PictureBox2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox2.Paint
        'Debug.Write(vbCr & strwidth.Width & vbCr)
        'Debug.Write(String.Format("widthX = {0}", CStr(widthX) & vbCr))
        ' Debug.Write("Dette er str: " & str)
        SetStyle(ControlStyles.AllPaintingInWmPaint Or _
            ControlStyles.OptimizedDoubleBuffer Or _
            ControlStyles.UserPaint, True)

        e.Graphics.Clear(Me.PictureBox2.BackColor)
        e.Graphics.DrawString(str, fo, Brushes.Blue, widthX, heightY)
        e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        ' e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighSpeed




        

        ' Debug.Write(String.Format("the string width is {0}", CStr(strwidth.Width)))
        If widthX <= (0 - douX) Then
            widthX = Me.PictureBox2.Width

            ' Vi laster inn nyheter p� nytt slik at vi f�r de siste....
            Me.loadthenews()
        Else
            widthX -= TickerSpeed
        End If

        e.Dispose()

    End Sub
    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        init()


    End Sub

    Private Sub init()
        Me.Width = Screen.PrimaryScreen.WorkingArea.Width
        Me.Location = New Point(0, 100)


        ' Her finner vi h�yden p� skjemaet og p� bildet. 
        If My.Settings.RSSFontSize = Nothing Then
            FontSize = 60
            My.Settings.RSSFontSize = FontSize
        Else
            FontSize = My.Settings.RSSFontSize
        End If



        Me.Height = FontSize * 2
        Me.PictureBox2.Height = Me.Height

        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Text = ""
        Me.Icon = Nothing
        Me.MaximumSize = New Point(Screen.PrimaryScreen.WorkingArea.Width, Me.Height)
        ' Plasserer ticker nederst p� skjermen. 
        Me.Top = Screen.PrimaryScreen.WorkingArea.Height - Me.Height
        ' Me.TransparencyKey = Me.BackColor
        '        Me.Opacity = 0.5

        PictureBox2.Left = Me.Left
        PictureBox2.Width = Me.Width
        g = Me.PictureBox2.CreateGraphics 'get the graphics object of the form

        widthX = Me.PictureBox2.Width        ' x co- ordinate or width
        heightY = Me.PictureBox2.Height / 2 - FontSize
        ' Debug.Write(String.Format("The width is {0}", �CStr(Me.Width)))


        Me.loadthenews()
        ' Dim gr As Graphics
        If TickerSpeed = 0 Then
            TickerSpeed = 5
            My.Settings.TickerSpeed = TickerSpeed
        End If
        Timer1.Interval = TickerSpeed
        Timer1.Start()

        fo = New Font("ARIAL", FontSize, FontStyle.Bold, GraphicsUnit.Point)
        strwidth = g.MeasureString(str, fo)
        douX = strwidth.Width
    End Sub

    Private Sub loadthenews()
        ' Create an empty instance of the NetworkCredential class.
        ' Dim rssFeedPath As String = "\\bhutan\d$\RSS\1881\RSS\RSSFeed.xml"

        Dim resolver As XmlUrlResolver = New XmlUrlResolver()
        Dim credentials As NetworkCredential = New NetworkCredential(UserName, PassWord)

        resolver.Credentials = credentials

        Dim docXML As New XmlDocument
        Try
            docXML.XmlResolver = resolver
        Catch ex As Exception
            Debug.Write(ex.ToString)
        End Try


        ' Getting the Document

        Try
            docXML.Load(My.Settings.RSSServer)
        Catch ex As Exception
            Debug.Write(ex.ToString)

        End Try

        Dim Nodes As XmlNodeList


        Nodes = docXML.SelectNodes("//channel/item/description")
        Dim title As String = docXML.SelectSingleNode("//channel/item/description").InnerText
        '        str = " * " & title
        If Nodes.Count <> 0 Then
            Dim x = Nodes.Count
            Dim y
            For y = 0 To x - 1
                str += " * " & Nodes.Item(y).InnerText.ToString()

            Next
        End If





    End Sub


    Private Sub frmNewsTicker_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseUp, PictureBox2.MouseUp
        Me.bFormDragging = False
    End Sub

    Private Sub frmNewsTicker_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove, PictureBox2.MouseMove
        If Me.bFormDragging Then
            Dim oMoveToPoint As Point

            ' Use the current mouse position to find 
            ' the target location
            oMoveToPoint = Me.PointToScreen(New Point(e.X, e.Y))
            ' Adjust the position based on where you started
            oMoveToPoint.Offset(Me.oPointClicked.X * -1, _
               (Me.oPointClicked.Y + _
               SystemInformation.CaptionHeight + _
               SystemInformation.BorderSize.Height) * -1)

            ' Move the form
            Me.Location = oMoveToPoint
        End If
    End Sub

    Private Sub frmNewsTicker_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseDown, PictureBox2.MouseDown
        ' Skal vi pr�ve � dra den litt da?

        Me.bFormDragging = True
        Me.oPointClicked = New Point(e.X, e.Y)
    End Sub

    

    Private Sub tsbRestart_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Denne skal restarte "skiten"
        Try
            Timer1.Stop()
            str = Nothing
            Me.init()
            Timer1.Start()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try


    End Sub

   

   
    Private Sub InnstillingerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InnstillingerToolStripMenuItem.Click
        Timer1.Stop()

        frmConfig.ShowDialog()


        If frmConfig.ChangedConfig = True Then

            TickerSpeed = My.Settings.TickerSpeed
            If Not mFontSize = Nothing Then
                mFontSize = My.Settings.RSSFontSize
            End If

            Try
                str = Nothing
                Me.init()
                Timer1.Start()
            Catch ex As Exception
                Debug.Write(ex.ToString)
            End Try

        Else
            Timer1.Start()

        End If
    End Sub

    Private Sub AvsluttToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AvsluttToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.PictureBox2.Refresh()
    End Sub
End Class
