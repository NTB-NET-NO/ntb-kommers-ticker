
Imports System.net
Imports System.Text
Imports System.Globalization




Public Class frmConfig
    Public UserName As String = My.Settings.RSSUserName
    Public PassWord As String = My.Settings.RSSPassWord
    Public RssPath As String = My.Settings.RSSServer

    Dim enUSCulture As New CultureInfo("en-US", True)

    Public TickerSpeed As Single = Val(My.Settings.TickerSpeed) * 10

    Public SavedConfig As Boolean
    Public ChangedConfig As Boolean


    Private Sub frmConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim a As Integer

        For a = 10 To 100
            cmbFontSize.Items.Add(a.ToString)
            If My.Settings.RSSFontSize = "" Then
                My.Settings.RSSFontSize = 60
            End If
            If a = My.Settings.RSSFontSize Then
                cmbFontSize.SelectedIndex = a - 10
                'cmbFontSize.Update()
            End If

        Next

        SavedConfig = False
        ChangedConfig = False


        If TickerSpeed = 0 Then
            TickerSpeed = 50
        End If
        trackSpeed.Value = TickerSpeed / 10

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        SavedConfig = True
        ChangedConfig = False
        My.Settings.Reload()
        Me.Close()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        
        My.Settings.TickerSpeed = trackSpeed.Value
        My.Settings.RSSFontSize = cmbFontSize.Text

        My.Settings.Save()
        SavedConfig = True
        ChangedConfig = True

        Me.Close()

    End Sub
End Class